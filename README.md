# Product Detection #

In this [competition](https://www.kaggle.com/c/shopee-product-detection-open), a multiple image classification model needs to be built. There are ~100k images within 42 different categories, including essential medical tools like masks, protective suits and thermometers, home & living products like air-conditioner and fashion products like T-shirts, rings, etc. For the data security purpose the category names will be desensitized. The evaluation metrics is top-1 accuracy.


![Loss](image/products.png)

### Method ###

1. We use [EfficientNetB4](https://arxiv.org/abs/1905.11946) as the backbone and few layers of feedforwad network as the classfiier. 

2. [AdamW](https://www.fast.ai/2018/07/02/adam-weight-decay/) is used as the optimizer. It's a better version Adam optimzier.

3.  We use [StratifiedKFold](https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.StratifiedKFold.html) to split the training and validation data. The we average the output model trained on each k-fold to get the final result. 


### Result ###

Figure below show the loss plot during training.

![Loss](image/loss_plot.png)

Our model achieve the accuracy of 81.35 %.

### Congratulation ###

We need to appreciate our little work. We still get 90th out of 646 teams. Yeaay, good job our team!! 

